package plugins.praveen.fft;

import icy.sequence.Sequence;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarSequence;
import plugins.praveen.fft.FFTCalculator.FFTDims;
import plugins.praveen.fft.FFTCalculator.FFTOutputType;

public class FFT extends EzPlug implements Block {

	EzVarSequence input = new EzVarSequence("Input");
	EzVarEnum<FFTDims> ndims =  new EzVarEnum<FFTDims>("Type", FFTDims.values(), 0);
	EzVarEnum<FFTOutputType> outputType =  new EzVarEnum<FFTOutputType>("Output as", FFTOutputType.values(), 0);
	EzVarBoolean	swap = new EzVarBoolean("Swap Quadrants?", false);
	
	VarSequence fSequenceVar = new VarSequence("FFT sequence", null);

	@Override
	protected void initialize() {
		super.addEzComponent(input);
		super.addEzComponent(ndims);
		super.addEzComponent(outputType);
		super.addEzComponent(swap);
		super.setTimeDisplay(true);
	}
	
	// declare ourself to Blocks
	@Override
	public void declareInput(VarList inputMap) {
		inputMap.add(input.name, input.getVariable());
		inputMap.add(ndims.name, ndims.getVariable());
		inputMap.add(outputType.name, outputType.getVariable());
		inputMap.add(swap.name, swap.getVariable());
	}

	// declare ourself to Blocks
	@Override
	public void declareOutput(VarList outputMap) {
		outputMap.add(fSequenceVar.getName(), fSequenceVar);
	}

	@Override
	protected void execute() {
		Sequence sequence = input.getValue();
		Sequence fSequence = null;

		if(ndims.getValue()==FFTDims.FFT_2D)
		{		
			fSequence = FFTCalculator.FFT_2D(sequence, swap.getValue(), outputType.getValue());
		}
		else
		{
			if (sequence.getSizeZ() >= 2)
			{
				fSequence = FFTCalculator.FFT_3D(sequence, swap.getValue(), outputType.getValue());
			}
			else
			{
				System.err.println("Sequence depth is 1, so computing 2D FFT instead of 3D.");
				fSequence = FFTCalculator.FFT_2D(sequence, swap.getValue(), outputType.getValue());
			}
		}
		
		if (!isHeadLess()) {
			addSequence(fSequence);
		}
		
		fSequenceVar.setValue(fSequence);
	}

	

	@Override
	public void clean() {
	}
}